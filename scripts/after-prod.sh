#!/bin/bash
client_jar="microservice-canal-boot-PROJECT-VERSION.jar";
rutaFinal="/opt/execute/";
profile="prod";
loggin="/opt/zum/microservice-canal/config/logback.xml";
config="http://user:s3cret@10.0.4.160:8888";
eureka="http://10.0.4.160:8761";
remove_last_jar="/opt/execute/microservice-canal-boot*";
rm $remove_last_jar;
cd "/opt/aws-store/microservice-canal/";
cp $client_jar $rutaFinal;

nohup java -jar $client_jar --spring.profiles.active=$profile --logging.config=$loggin --spring.cloud.config.uri=$config --eureka.client.serviceUrl.defaultZone=$eureka > /dev/null 2> /dev/null < /dev/null &


echo "Finalizado";