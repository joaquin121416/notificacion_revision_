#!/bin/bash
for i in $(ps aux | grep -e microservice-canal-boot | awk '{ print $2 }'  | head)
do
    #echo $i
    {
       kill  -9 $i;
    } || {
        echo "dosent kill process $i"
    }
done