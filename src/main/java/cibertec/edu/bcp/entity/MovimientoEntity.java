package cibertec.edu.bcp.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

public class MovimientoEntity {

	  @Id
	  @GeneratedValue(strategy=GenerationType.AUTO)
	  private Long noti_iident;
	  private String noti_vdescr;
	  private String noti_vdetail;
	  
	  @OneToOne
	  private MovimientoEntity mov_Entity;
	  
	  
	  @OneToOne
	  private PersonaEntity pers_Entity;
	
	
}
