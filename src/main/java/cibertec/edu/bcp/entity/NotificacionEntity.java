package cibertec.edu.bcp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_notificacion")
public class NotificacionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long noti_iident;
	private String noti_vdescr;
	private String noti_vdetail;

	public Long getNoti_iident() {
		return noti_iident;
	}

	public void setNoti_iident(Long noti_iident) {
		this.noti_iident = noti_iident;
	}

	public String getNoti_vdescr() {
		return noti_vdescr;
	}

	public void setNoti_vdescr(String noti_vdescr) {
		this.noti_vdescr = noti_vdescr;
	}

	public String getNoti_vdetail() {
		return noti_vdetail;
	}

	public void setNoti_vdetail(String noti_vdetail) {
		this.noti_vdetail = noti_vdetail;
	}

}
