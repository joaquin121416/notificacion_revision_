package cibertec.edu.bcp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cibertec.edu.bcp.entity.NotificacionEntity;
import cibertec.edu.bcp.repository.NotificacionRepository;
import cibertec.edu.bcp.service.NotificacionService;



@Service
public class NotificacionServiceImpl implements NotificacionService{

	
	@Autowired
	private NotificacionRepository notificacionRepository;
 
	@Override
	public NotificacionEntity insert(NotificacionEntity notificacion) {
	
		return notificacionRepository.save(notificacion);
	}

	@Override
	public NotificacionEntity update(NotificacionEntity notificacion) {

		return notificacionRepository.save(notificacion);
	}
 
	@Override
	public Integer delete(Long id) {
	
		notificacionRepository.deleteById(id);
		Optional<NotificacionEntity> emp = notificacionRepository.findById(id);
		if (emp.isPresent()) {
			return -1;
		}
		return 1;
	}

	@Override
	public Optional<NotificacionEntity> getById(Long id) {

		return notificacionRepository.findById(id);
	}

	@Override
	public List<NotificacionEntity> get() {
		
		return (List<NotificacionEntity>) notificacionRepository.findAll();
	}


}
