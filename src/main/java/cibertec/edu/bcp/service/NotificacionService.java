package cibertec.edu.bcp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import cibertec.edu.bcp.entity.NotificacionEntity;



public interface NotificacionService {
	
	NotificacionEntity insert(NotificacionEntity notificacion);

	NotificacionEntity update(NotificacionEntity notificacion);

	Integer delete(Long noti_iident);

	Optional<NotificacionEntity> getById(Long noti_iident);

	List<NotificacionEntity> get();
}
