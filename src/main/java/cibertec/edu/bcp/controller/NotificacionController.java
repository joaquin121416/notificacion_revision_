package cibertec.edu.bcp.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import cibertec.edu.bcp.entity.NotificacionEntity;
import cibertec.edu.bcp.service.impl.NotificacionServiceImpl;
import io.swagger.annotations.ApiParam;




@RestController
@RequestMapping("/api/v1/")
public class NotificacionController {

	@Autowired
	private NotificacionServiceImpl notificacionServiceImpl;

	@GetMapping(value = "/notify", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NotificacionEntity> listNotificacion() {
		return notificacionServiceImpl.get();
	}

	@GetMapping(value = "/notify/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public NotificacionEntity obtenerNotificacion(//@ApiParam(value = "Id de la notificacion", required = true) 
	@PathVariable("id") Long id) {
		return notificacionServiceImpl.getById(id).get();
	}

	@PostMapping(value = "/notify" , consumes  = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(//@ApiParam(value = "Id de la notificacion", required = true)
	@RequestBody NotificacionEntity notificacion) {

		NotificacionEntity notificacionSave = notificacionServiceImpl.insert(notificacion);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(notificacionSave.getNoti_iident()).toUri();

		return ResponseEntity.ok(notificacionSave);
	}

	@PutMapping(value = "/notify/{id}")
	public ResponseEntity<Object> actualizar(@RequestBody NotificacionEntity notificacion,@ApiParam(value = "Id de la notificacion", required = true) @PathVariable("id") Long id) {
		notificacion.setNoti_iident(id);
		notificacionServiceImpl.update(notificacion);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping(value = "/notify/{id}")
	public void eliminar(@ApiParam(value = "Id de la notificacion", required = true) @PathVariable("id") Long id) {
		notificacionServiceImpl.delete(id);
	}

	
	
}
