package cibertec.edu.bcp.repository;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.repository.CrudRepository;


import cibertec.edu.bcp.entity.NotificacionEntity;


public interface NotificacionRepository extends CrudRepository<NotificacionEntity, Long>{

}
